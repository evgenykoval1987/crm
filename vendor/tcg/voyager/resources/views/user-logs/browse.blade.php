@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            {{ $dataType->display_name_plural }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @if ($data->type == 1)
                                            <td style="color: green">
                                                {{$data->text}}
                                            </td>
                                        @else
                                            <td style="color: blue">
                                                {{$data->text}}
                                            </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "search" => ['caseInsensitive' => false],
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});

                table.column(9).visible(false);
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });




            $('#id, #client').keyup( function() { 
                table.draw();
            } );
            $("#clinic, #vet_id").change( function() {
                table.draw();
            } );

            $(".dtpic").datetimepicker({"locale": "ru","format": "DD.MM.YYYY", "useCurrent" : false}).on('dp.change',function(){
                table.draw();
            })
        });

        
        

        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var fields = [];
                var id = $('#id').val();
                var client = $('#client').val();
                var clinic = $('#clinic').val();
                var vet = $('#vet_id').val();
                var date_from = $('#date_from').val();
                var date_to = $('#date_to').val();
                console.log(date_from);
                var table_id = data[1];
                var table_client = data[5];
                var table_clinic = data[3];
                var table_vet = data[9];
                var table_dt = data[4];

                //console.log(data);
                if (id != ''){
                    if (table_id.indexOf(id) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (client != ''){
                    if (table_client.toLowerCase().indexOf(client) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (clinic != ''){
                    if (table_clinic.indexOf(clinic) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (vet != ''){
                    if (table_vet.indexOf(vet) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (date_from != ''){
                    td = Date.parse(table_dt);
                    df = Date.parse(date_from);
                    
                    if (td > df){ 
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (date_to != ''){
                    td = Date.parse(table_dt);
                    dt = Date.parse(date_to);
                    if (td <= dtdate_to){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }

                var res = true;
                for (field in fields){
                    if (!fields[field])
                        res = false;
                }

                return res;
            }
        );

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
@stop
