@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle" data-on="{{ __('voyager::bread.soft_deletes_off') }}" data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach(Voyager::actions() as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div style="padding-left: 15px">
                            <div class="row">
                                <div class="col-sm-12"><h4>Фильтр</h4></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="id">Номер</label>
                                        <input type="text" class="form-control" id="id" name="id">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="client">Клиент</label>
                                        <input type="text" class="form-control" id="client" name="client">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="clinic">Канал</label>
                                        @php
                                            $model = app('TCG\\Voyager\\Models\\Clinic');
                                            $clinics = $model->all();
                                        @endphp
                                        <select id="clinic" name="clinic" class="form-control">
                                            <option></option>
                                            @foreach($clinics as $clinic)
                                                <option value="{{$clinic->title}}">{{$clinic->title}}</option>
                                            @endforeach
                                        </select>     
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="vet_id">Ветеринар</label>
                                        @php
                                            $model = app('TCG\\Voyager\\Models\\Vet');
                                            $vets = $model->all();
                                        @endphp
                                        <select id="vet_id" name="vet_id" class="form-control">
                                            <option></option>
                                            @foreach($vets as $vet)
                                                <option value="{{$vet->name}}">{{$vet->name}}</option>
                                            @endforeach
                                        </select>     
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="date_from">Дата с</label>
                                        <input type="text" class="form-control dtpic" id="date_from" name="date_from">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="date_to">Дата по</label>
                                        <input type="text" class="form-control dtpic" id="date_to" name="date_to">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="vet_id">Статус заказа</label>
                                        @php
                                            $model = app('TCG\\Voyager\\Models\\Statustype');
                                            $statuses = $model->all();
                                        @endphp
                                        <select id="status_id" name="status_id" class="form-control select2" multiple>
                                            <option></option>
                                            @foreach($statuses as $status)
                                                <option value="{{$status->title}}">{{$status->title}}</option>
                                            @endforeach
                                        </select>     
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <select id="search_key" name="key">
                                        @foreach($searchable as $key)
                                            <option value="{{ $key }}" @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)){{ 'selected' }}@endif>{{ ucwords(str_replace('_', ' ', $key)) }}</option>
                                        @endforeach
                                    </select>
                                    <select id="filter" name="filter">
                                        <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>contains</option>
                                        <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
                                    </select>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                @if (Request::has('sort_order') && Request::has('order_by'))
                                    <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                                    <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                                @endif
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <th>
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endcan
                                        @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                            @endif
                                            {{ $row->display_name }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField($orderBy))
                                                    @if ($sortOrder == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endforeach
                                        <th>Канал</th>
                                        <th>Дата создания</th>
                                        <th>Клиент</th>
                                        <th>Телефон</th>
                                        <th>Адрес</th>
                                        <th>Состав</th>
                                        <th>Ветеринар</th>
                                        <th>Сумма</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endcan
                                        <td>
                                            <a href="{{route('voyager.orders.edit', $data->id)}}">
                                                {{$data->id}}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="badge badge-xs" style="background-color: {{$data->status->color}}">{{$data->status->title}}</span>
                                        </td>
                                        <td>
                                            {{$data->clinic->title}}
                                        </td>
                                        <td>
                                            {{date('d.m.Y H:i',strtotime($data->created_at))}}
                                        </td>
                                        <td>
                                            {{$data->client}}
                                        </td>
                                        <td>
                                            @php
                                                $telephones = explode('|', $data->telephone);
                                                $telephones = implode('<br>', $telephones);
                                            @endphp
                                            {!!$telephones!!}
                                        </td>
                                        <td>
                                            @php
                                                $address = [];
                                                if ($data->address_city != '')
                                                    $address[] = "г.".$data->address_city;
                                                if ($data->address_street != '')
                                                    $address[] = $data->address_street;
                                                if ($data->address_house != '')
                                                    $address[] = "д.".$data->address_house;
                                                if ($data->address_corpus != '')
                                                    $address[] = "к.".$data->address_corpus;
                                                if ($data->address_porch != '')
                                                    $address[] = "п.".$data->address_porch;
                                                if ($data->address_floor != '')
                                                    $address[] = "эт.".$data->address_floor;
                                                if ($data->address_intercom != '')
                                                    $address[] = "дмф.".$data->address_intercom;
                                                
                                                $address = implode(', ', $address)
                                            @endphp
                                            {{$address}}
                                        </td>
                                        <td>
                                            @php

                                                $products = [];
                                                foreach ($data->products as $product){
                                                    $products[] = $product->product->title;
                                                }
                                                $products = implode('<br>', $products)
                                            @endphp
                                            {!!$products!!}
                                        </td>
                                        
                                        <td>
                                            @if($data->vet)
                                                {{$data->vet->name}}
                                            @else

                                            @endif

                                        </td>
                                        <td>
                                            {{$data->total($data->id)}} руб.
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder,
                                    'showSoftDeleted' => $showSoftDeleted,
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "search" => ['caseInsensitive' => false],
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});

                table.column(9).visible(false);
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });




            $('#id, #client').keyup( function() { 
                table.draw();
            } );
            $("#clinic, #vet_id, #status_id").change( function() {
                table.draw();
            } );

            $(".dtpic").datetimepicker({"locale": "ru","format": "DD.MM.YYYY", "useCurrent" : false}).on('dp.change',function(){
                table.draw();
            })
        });

        
        

        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var fields = [];
                var id = $('#id').val();
                var client = $('#client').val();
                var clinic = $('#clinic').val();
                var vet = $('#vet_id').val();
                var date_from = $('#date_from').val();
                var date_to = $('#date_to').val();
                var status = $('#status_id').val();

                var table_id = data[1];
                var table_status = data[2];
                var table_client = data[5];
                var table_clinic = data[3];
                var table_vet = data[9];
                var table_dt = data[4];

                
                if (status != ''){
                    var in_status = false;
                    for (i in status){
                        if (table_status.indexOf(status[i]) !== -1){
                            in_status = true;
                        }
                    }
                    if (in_status){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }

                if (id != ''){
                    if (table_id.indexOf(id) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (client != ''){
                    if (table_client.toLowerCase().indexOf(client) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (clinic != ''){
                    if (table_clinic.indexOf(clinic) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (vet != ''){
                    if (table_vet.indexOf(vet) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (date_from != ''){
                    td = Date.parse(table_dt);
                    df = Date.parse(date_from);
                    
                    if (td > df){ 
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                if (date_to != ''){
                    td = Date.parse(table_dt);
                    dt = Date.parse(date_to);
                    if (td <= dtdate_to){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }

                var res = true;
                for (field in fields){
                    if (!fields[field])
                        res = false;
                }

                return res;
            }
        );

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
@stop
