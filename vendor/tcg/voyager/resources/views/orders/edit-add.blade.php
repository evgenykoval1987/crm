@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
    if ($add)
    	$dataTypeContent->id = 0;
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style type="text/css">
    	.selected-product{
    		background-color: #e3ffea!important;
    	}
    </style>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">
            					<div class="col-md-6">
            						<div class="form-group">
		                            	<label for="default_statustype">Тип заказа</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_ordertype_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('ordertype_id', $dataTypeContent->ordertype_id ?? '') != '')
		                                    	$dataTypeContent->ordertype_id = old('ordertype_id', $dataTypeContent->ordertype_id ?? '');
		                                    else if(!$dataTypeContent->id)
		                                    	$dataTypeContent->ordertype_id = 1;
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                            </div>
		                            <div class="form-group {{ $errors->has('project_id') ? 'has-error' : '' }}">
		                            	<label for="default_statustype">Проект</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_project_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('project_id', $dataTypeContent->project_id ?? '') != '')
		                                    	$dataTypeContent->project_id = old('project_id', $dataTypeContent->project_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                                @if ($errors->has('project_id'))
	                                        @foreach ($errors->get('project_id') as $error)
	                                            <span class="help-block">{{ $error }}</span>
	                                        @endforeach
	                                    @endif
		                            </div>
		                            <div class="form-group {{ $errors->has('clinic_id') ? 'has-error' : '' }}">
		                            	<label for="default_statustype">Партнер</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_clinic_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('clinic_id', $dataTypeContent->clinic_id ?? '') != '')
		                                    	$dataTypeContent->clinic_id = old('clinic_id', $dataTypeContent->clinic_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                                @if ($errors->has('clinic_id'))
	                                        @foreach ($errors->get('clinic_id') as $error)
	                                            <span class="help-block">{{ $error }}</span>
	                                        @endforeach
	                                    @endif
		                            </div>
		                            <div class="form-group">
		                            	<label for="default_statustype">Способ оформления</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_executiontype_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('executiontype_id', $dataTypeContent->executiontype_id ?? '') != '')
		                                    	$dataTypeContent->executiontype_id = old('executiontype_id', $dataTypeContent->executiontype_id ?? '');
		                                    else if(!$dataTypeContent->id)
		                                    	$dataTypeContent->executiontype_id = 1;
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                            </div>
            					</div>
            					<div class="col-md-6">
            						<div class="form-group {{ $errors->has('status_id') ? 'has-error' : '' }}">
		                            	<label for="default_statustype">Статус</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_statustype_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('status_id', $dataTypeContent->status_id ?? '') != '')
		                                    	$dataTypeContent->status_id = old('status_id', $dataTypeContent->status_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                                @if ($errors->has('status_id'))
	                                        @foreach ($errors->get('status_id') as $error)
	                                            <span class="help-block">{{ $error }}</span>
	                                        @endforeach
	                                    @endif
		                            </div>
		                            @section('submit-buttons')
		                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
		                            @stop
		                            @yield('submit-buttons')
            					</div>
            				</div>
                            <hr>
                            <div class="row">
                            	<div class="col-md-12"><h3>Клиент</h3></div>
            					<div class="col-md-6">
            						<div class="form-group">
		                                <label for="client">ФИО</label>
		                                <input type="text" class="form-control" id="client" name="client" placeholder="ФИО"
		                                      value="{{ old('client', $dataTypeContent->client ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-6">
            						<div class="form-group">
            							<label>Телефон</label>

            							
    									@if($dataTypeContent->telephone != '' || old('telephone', $dataTypeContent->telephone ?? '') != '')
	            							@php
	            								if (old('telephone', $dataTypeContent->telephone ?? '') != ''){
	            									$telephones = old('telephone', $dataTypeContent->telephone ?? '');
	            									$telephones = explode('|',$telephones);
	            								}
	            								else
	            									$telephones = explode('|',$dataTypeContent->telephone);
	            							@endphp

	            							@foreach($telephones as $telephone)
	            								@if($loop->index != 0)
		            								<div class="input-group" style="margin-bottom: 10px;">
											      		<input type="text" class="form-control" name="telephones[]" value="{{$telephone}}" data-mask="+7 (000) 000 00 00" placeholder="+7 (___) ___ __ __">
												      	<span class="input-group-btn">
												        	<button class="btn btn-danger btn-sm remove-phone" type="button" style="margin-top: 0px; margin-bottom: 0px"><i class="voyager-trash"></i></button>
												      	</span>
												    </div>
												@else
													<div class="form-group"  tyle="margin-bottom: 10px;">
											      		<input type="text" class="form-control" name="telephones[]" value="{{$telephone}}">
												    </div>
												@endif
	            							@endforeach
	            						@else
	            							<div class="form-group"  tyle="margin-bottom: 10px;">
									      		<input type="text" class="form-control" name="telephones[]" data-mask="+7 (000) 000 00 00" placeholder="+7 (___) ___ __ __">
										    </div>		
	            						@endif
	            						<button type="button" class="btn btn-sm btn-primary" id="addphone"><i class="voyager-plus" ></i></button>
	            					</div>
            					</div>
            				</div>
            				<div class="row">
            					<div class="col-md-12"><h3>Адрес</h3></div>
            					<div class="col-md-6">
            						<div class="form-group">
		                                <label for="address_city">Город</label>
		                                <input type="text" class="form-control" id="address_city" name="address_city" placeholder="Город"
		                                       value="{{ old('address_city', $dataTypeContent->address_city ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-6">
            						<div class="form-group">
		                                <label for="address_street">Улица</label>
		                                <input type="text" class="form-control" id="address_street" name="address_street" placeholder="Улица"
		                                       value="{{ old('address_street', $dataTypeContent->address_street ?? '') }}">
		                            </div>
            					</div>
            				</div>
            				<div class="row">
            					<div class="col-md-2">
            						<div class="form-group">
		                                <label for="address_house">Дом</label>
		                                <input type="text" class="form-control" id="address_house" name="address_house" placeholder="Дом" value="{{ old('address_house', $dataTypeContent->address_house ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-2">
            						<div class="form-group">
		                                <label for="address_corpus">Корпус/строение</label>
		                                <input type="text" class="form-control" id="address_corpus" name="address_corpus" placeholder="Корпус/строение" value="{{ old('address_corpus', $dataTypeContent->address_corpus ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-2">
            						<div class="form-group">
		                                <label for="address_porch">Подъезд</label>
		                                <input type="text" class="form-control" id="address_porch" name="address_porch" placeholder="Подъезд" value="{{ old('address_porch', $dataTypeContent->address_porch ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-2">
            						<div class="form-group">
		                                <label for="address_floor">Этаж</label>
		                                <input type="text" class="form-control" id="address_floor" name="address_floor" placeholder="Этаж" value="{{ old('address_floor', $dataTypeContent->address_floor ?? '') }}">
		                            </div>
            					</div>
            					<div class="col-md-2">
            						<div class="form-group">
		                                <label for="address_intercom">Домофон</label>
		                                <input type="text" class="form-control" id="address_intercom" name="address_intercom" placeholder="Домофон" value="{{ old('address_intercom', $dataTypeContent->address_intercom ?? '') }}">
		                            </div>
            					</div>
            				</div>
            				<div class="row">
            					<div class="col-md-6">
            						<div class="form-group">
		                                <label for="ocomment">Комментарий оператора</label>
		                                <textarea class="form-control" id="ocomment" name="ocomment" placeholder="Комментарий оператора" rows="5">{{ old('ocomment', $dataTypeContent->ocomment ?? '') }}</textarea>
		                                
		                            </div>
            					</div>
            				</div>
            				@if ($dataTypeContent->telegram_status != '')
            					<input type="hidden" name="telegram_status" value="{{ $dataTypeContent->telegram_status }}">
            				@endif
            				<div class="row">
                            	<div class="col-md-12"><h3>Отложенный заказ</h3></div>
                            	<div class="col-md-6">
            						<div class="form-group">
		                                <label for="deferred_dt">Дата</label>
		                                @if ($dataTypeContent->deferred_dt != '') 
		                                	<input type="datetime" class="form-control datepicker" name="deferred_dt"
       value="@if(isset($dataTypeContent->deferred_dt)){{ \Carbon\Carbon::parse($dataTypeContent->deferred_dt)->format('Y-m-d') }}@else{{''}}@endif">
		                                @else
		                                	<input type="datetime" class="form-control datepicker" name="deferred_dt"
       value="@if(old('deferred_dt', $dataTypeContent->deferred_dt ?? '') != ''){{ \Carbon\Carbon::parse(old('deferred_dt', $dataTypeContent->deferred_dt ?? ''))->format('Y-m-d') }}@else{{''}}@endif">
		                                @endif
		                                
		                            </div>
            					</div>
            					<div class="col-md-6">
            						<div class="form-group">
		                            	<label for="deferred_time">Время</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_time_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('deferred_time_id', $dataTypeContent->deferred_time_id ?? '') != '')
		                                    	$dataTypeContent->deferred_time_id = old('deferred_time_id', $dataTypeContent->deferred_time_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                            </div>
            					</div>
                            </div>
                            <hr>
                            <div class="row">
                            	<div class="col-md-12"><h3>Состав заказа <button type="button" class="btn btn-sm btn-primary" id="add-product-button"><i class="voyager-plus"></i> Добавить услугу</button></h3> </div>
                            	
                            	<div class="col-md-12">
                            		<table class="table table-striped" id="dataTable">
	                            		<thead>
	                            			<tr>
	                            				<th>Услуги</th>
	                            				<th>Цена</th>
	                            				<th>Удалить</th>
	                            			</tr>
	                            		</thead>
	                            		<tbody>
	                            			@php
	                            				$sproducts = [];
							                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
							                    $row     = $dataTypeRows->where('field', 'order_hasmany_order_product_relationship')->first();
	                            				$options = $row->details;
							                    $model = app($options->model);

							            		$products = $model::where($options->column, '=', $relationshipData->id)->get();
							            		if (count($products) <= 0){
							            			if (old('products', $dataTypeContent->products ?? '') != '') 
		                                    			$products = $dataTypeContent->products = old('products', $dataTypeContent->products ?? '');
							            		}
							            		if (count($products) <= 0 && Session::has('order_products')){
							            			$model = app('TCG\Voyager\Models\Product');
							            			$sproducts = $model::whereIn('id',Session::get('order_products'))->get();
							            			$total = 0;
							            			foreach ($model::whereIn('id',Session::get('order_products'))->get() as $key => $product) {
							            				if ($product->special)
							            					$total += $product->special;
							            				else
							            					$total += $product->price;
							            			}
							            			$discount = Session::get('order_discount');
							            			if (isset($discount['discount_sum']) && $discount['discount_sum']){
							            				$total -= $discount['discount_sum'];
							            			}
							            			if (isset($discount['discount_percent']) && $discount['discount_percent']){
							            				$total -= ($discount['discount_percent']/100)*$total; 
							            			}
							            			//dd($total);
							            			//$total = $model::whereIn('id',Session::get('order_products'))->sum('price');
							            		}
							            		else if (count($products) > 0){
							            			$total = $dataTypeContent->total($dataTypeContent->id);
							            			if ($dataTypeContent->discount_sum)
							            				$total -= $dataTypeContent->discount_sum;
							            			if ($dataTypeContent->discount_percent)
							            				$total -= ($dataTypeContent->discount_percent/100)*$total;
							            		}
							                @endphp

							                @if(count($products) > 0)
							                	@foreach($products as $product)

							                		<tr>
							                			<td>{{$product->product->title}}<input type="hidden" name="productss[]" value="{{$product->product->category_id}}"></td>
							                			<td>{{$product->product->price}}</td>
							                			<td id="bread-actions">
							                				<a href="javascript:;" class="btn btn-sm btn-danger remove-product" data-id="{{$product->product->product_id}}" data-order-id="{{$dataTypeContent->id}}">
                    											<i class="voyager-trash"></i> <span class="hidden-xs hidden-sm"></span>
                											</a>
                										</td>
							                		</tr>
							                	@endforeach
							                @elseif(count($sproducts) > 0)
							                	@foreach($sproducts as $product)
							                		<tr>
							                			<td>{{$product->title}}<input type="hidden" name="productss[]" value="{{$product->category_id}}"></td>
							                			<td>{{$product->price}}</td>
							                			<td id="bread-actions">
							                				<a href="javascript:;" class="btn btn-sm btn-danger remove-product" data-id="{{$product->id}}" data-order-id="0">
                    											<i class="voyager-trash"></i> <span class="hidden-xs hidden-sm"></span>
                											</a>
                										</td>
							                		</tr>
							                	@endforeach
							                @else
							                	<tr><td colspan="3" style="text-align: center;">Не назначено товаров.</td></tr>
							                @endif
	                            		</tbody>
	                            		@if(count($products) > 0)
	                            			<tfoot>
	                            				<tr>
	                            					<td colspan="1"></td>
	                            					<td colspan="1"><b>Итого</b></td>

	                            					<td colspan="1" style="text-align: right;"><b><span class="order-sum">{{$total}}</span> руб.</b></td>
	                            				</tr>
	                            				<tr>
	                            					<td colspan="1" style="text-align: right;"><b>Скидки на весь заказ</b></td>
	                            					<td colspan="1">
	                            						<div class="row">
	                            							<div class="col-md-6">
	                            								<div class="input-group">
																  	<input type="text" class="form-control" id="discount-sum" value="{{ $dataTypeContent->discount_sum ?? '' }}" aria-describedby="discount-sum">
																  	<span class="input-group-addon" id="discount-sum">руб.</span>
																</div>
							            					</div>
							            					<div class="col-md-6">
							            						<div class="input-group">
																  	<input type="text" class="form-control" id="discount-percent" value="{{ $dataTypeContent->discount_percent ?? '' }}" aria-describedby="discount-percent">
																  	<span class="input-group-addon" id="discount-percent">%</span>
																</div>
							            					</div>
	                            						</div>
	                            					</td>
	                            					<td colspan="1"><button type="button" class="btn btn-sm btn-warning" style="margin-top: 0px; margin-bottom: 0px;" id="add_discount">Применить</button></td>
	                            				</tr>
	                            			</tfoot>
	                            		@elseif(count($sproducts) > 0)
	                            			<tfoot>
	                            				<tr>
	                            					<td colspan="1"></td>
	                            					<td colspan="1" style="text-align: right;"><b>Итого</b></td>

	                            					<td colspan="1"><b><span class="order-sum">{{$total}}</span> руб.</b></td>
	                            				</tr>
	                            				<tr>
	                            					<td colspan="1" style="text-align: right;"><b>Скидки на весь заказ</b></td>
	                            					<td colspan="1">
	                            						<div class="row">
	                            							<div class="col-md-6">
	                            								<div class="input-group">
																  	<input type="text" class="form-control" id="discount-sum" value="{{ $discount['discount_sum'] ?? '' }}" aria-describedby="discount-sum">
																  	<span class="input-group-addon" id="discount-sum">руб.</span>
																</div>
							            					</div>
							            					<div class="col-md-6">
							            						<div class="input-group">
																  	<input type="text" class="form-control" id="discount-percent" value="{{ $discount['discount_percent'] ?? '' }}" aria-describedby="discount-percent">
																  	<span class="input-group-addon" id="discount-percent">%</span>
																</div>
							            					</div>
	                            						</div>
	                            					</td>
	                            					<td colspan="1"><button type="button" class="btn btn-sm btn-warning" style="margin-top: 0px; margin-bottom: 0px;" id="add_discount-live">Применить</button></td>
	                            				</tr>
	                            			</tfoot>
	                            		@else
	                            			<tfoot>
	                            				<tr>
	                            					<td colspan="1"></td>
	                            					<td colspan="1" style="text-align: right;"><b>Итого</b></td>

	                            					<td colspan="1"><b><span class="order-sum">0</span> руб.</b></td>
	                            				</tr>
	                            				<tr>
	                            					<td colspan="1" style="text-align: right;"><b>Скидки на весь заказ</b></td>
	                            					<td colspan="1">
	                            						<div class="row">
	                            							<div class="col-md-6">
	                            								<div class="input-group">
																  	<input type="text" class="form-control" id="discount-sum" value="0" aria-describedby="discount-sum">
																  	<span class="input-group-addon" id="discount-sum">руб.</span>
																</div>
							            					</div>
							            					<div class="col-md-6">
							            						<div class="input-group">
																  	<input type="text" class="form-control" id="discount-percent" value="0" aria-describedby="discount-percent">
																  	<span class="input-group-addon" id="discount-percent">%</span>
																</div>
							            					</div>
	                            						</div>
	                            					</td>
	                            					<td colspan="1"><button type="button" class="btn btn-sm btn-warning" style="margin-top: 0px; margin-bottom: 0px;" id="add_discount-live">Применить</button></td>
	                            				</tr>
	                            			</tfoot>
	                            		@endif

	                            	</table>
	                            </div>
                            </div>
                            <hr>
                            <div class="row">
                            	<div class="col-md-12"><h3>Данные по оказанию услуги</h3></div>
                            	
                            </div>
                            <div class="row">
                            	<div class="col-md-3">
                            		<div class="form-group">
		                            	<label for="vet_id">Ветеринар</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_user_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('vet_id', $dataTypeContent->vet_id ?? '') != '')
		                                    	$dataTypeContent->vet_id = old('vet_id', $dataTypeContent->vet_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                            </div>
                            	</div>
                            </div>
                            <div class="row">
                            	<div class="col-md-3">
                            		<div class="form-group">
		                                <label for="pay">Оплата поступила</label>
		                                <input type="text" class="form-control" id="pay" name="pay" placeholder="" value="{{ old('pay', $dataTypeContent->pay ?? '') }}">
		                            </div>
                            	</div>
                            	<div class="col-md-3">
                            		<div class="form-group">
		                            	<label for="payment_type_id">Способ оплаты</label>
		                                @php
		                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
		                                    $row     = $dataTypeRows->where('field', 'order_belongsto_payment_type_relationship')->first();
		                                    $options = $row->details;
		                                    if (old('payment_type_id', $dataTypeContent->payment_type_id ?? '') != '')
		                                    	$dataTypeContent->payment_type_id = old('payment_type_id', $dataTypeContent->payment_type_id ?? '');
		                                @endphp
		                                @include('voyager::formfields.relationship')
		                            </div>
                            	</div>
                            </div>
                            <div class="row">
                            	<div class="col-md-3">
                            		<div class="form-group">
		                                <label for="topay">К оплате клиентом</label>
		                                <input type="text" class="form-control" id="topay" name="topay" placeholder="" value="{{ old('topay', $dataTypeContent->topay ?? '') }}">
		                            </div>
                            	</div>
                            </div>
                            <div class="row">
                            	<div class="col-md-3">
                            		<div class="form-group">
		                                <label for="issue_dt">Заказ оформлен</label>
		                                <input type="datetime" class="form-control datepickert" name="issue_dt"
       value="@if(isset($dataTypeContent->issue_dt)){{ \Carbon\Carbon::parse($dataTypeContent->issue_dt)->format('Y-m-d H:i') }}@else{{''}}@endif" disabled="disabled">
		                            </div>
                            	</div>
                            	<div class="col-md-3">
                            		<div class="form-group">
		                                <label for="receive_dt">Заказ принят</label>
		                                <input type="datetime" class="form-control datepickert" name="receive_dt"
       value="@if(isset($dataTypeContent->receive_dt)){{ \Carbon\Carbon::parse($dataTypeContent->receive_dt)->format('Y-m-d H:i') }}@else{{''}}@endif"  disabled="disabled">
		                            </div>
                            	</div>
                            	<div class="col-md-3">
                            		<div class="form-group">
		                                <label for="close_dt">Заказ закрыт</label>
		                                <input type="datetime" class="form-control datepickert" name="close_dt"
       value="@if(isset($dataTypeContent->close_dt)){{ \Carbon\Carbon::parse($dataTypeContent->close_dt)->format('Y-m-d H:i') }}@else{{''}}@endif"  disabled="disabled">
		                            </div>
                            	</div>
                            </div>
                            <div class="row">
            					<div class="col-md-5">
            						<div class="form-group">
		                                <label for="vcomment">Комментарий ветеринара(при закрытии заказа)</label>
		                                <textarea class="form-control" id="vcomment" name="vcomment" rows="5">{{ old('vcomment', $dataTypeContent->vcomment ?? '')}}</textarea>
		                                
		                            </div>
            					</div>
            				</div>
            				<div class="row">
            					<div class="col-md-5">
            						<div class="form-group">
		                                <label for="video_email">Email для видео</label>
		                                <input type="text" class="form-control" id="video_email" name="video_email" placeholder="" value="{{ old('video_email', $dataTypeContent->video_email ?? '') }}">
		                            </div>
            					</div>
            				</div>
            				<div class="row">
            					<div class="col-md-5">
            						<div class="form-group">
		                                <label for="video_url">URL видео</label>
		                                <input type="text" class="form-control" id="video_url" name="video_url" placeholder="" value="{{ old('video_url', $dataTypeContent->video_url ?? '') }}">
		                            </div>
            					</div>
            				</div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>
                    @if(isset($dataTypeContent->id))
	                    <div class="panel-body">
	                    	<hr>
	                    	<div class="row">
	            				<div class="col-md-6">
	            					<h3>Лог заказа</h3>
	            				</div>
	            			</div>
	                    	@php
	                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
	                            $row     = $dataTypeRows->where('field', 'order_hasmany_order_log_relationship')->first();
	                            $options = $row->details;
	                            $model = app($options->model);
	                    		$logs = $model::where('order_id',$dataTypeContent->id)->ordered()->get();
	                        @endphp
	                        @if($logs)
		                        @foreach($logs as $log)
		                        	<div class="log-item" style="margin-bottom: 15px">
		                        		<span style="margin-right: 25px">{{date('d.m.Y H:i', strtotime($log->created_at))}}</span>
			                        	@if($log->from_status_id == 0)
			                        		<span class="badge badge-lg" style="background-color: #ccc">Создание</span>
			                        	@else
			                        		<span class="badge badge-lg" style="background-color: {{$log->fromstatus['color']}}">{{$log->fromstatus['title']}}</span>
			                        	@endif
			                        	<span>&nbsp;<i class="voyager-double-right"></i>&nbsp;</span>
			                        	<span class="badge badge-lg" style="background-color: {{$log->tostatus['color']}}">{{$log->tostatus['title']}}</span>
			                        	<hr>
		                        	</div>
		                        @endforeach
		                    @endif
	                    </div>
	                @endif

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-primary " id="product-list">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Выберите услуги/товары из списка</h4>
                </div>

                <div class="modal-body">
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-success" id="confirm_product">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $("#addphone").click(function(){
            	$(this).before('<div class="input-group" style="margin-bottom: 10px;"><input type="text" class="form-control newphone" name="telephones[]"><span class="input-group-btn"><button class="btn btn-danger btn-sm remove-phone" type="button" style="margin-top: 0px; margin-bottom: 0px" ><i class="voyager-trash"></i></button></span></div>');
            	$('.newphone').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __ __"});
            	return false;
            })

            $(document).on('click', ".remove-phone", function(){
            	$(this).parent().parent().remove();
            })

            $(document).on('click', ".remove-product", function(){
            	var params = {
            		order_id: $(this).data('order-id'),
            		product_id: $(this).data('id'),
            		_token: '{{ csrf_token() }}'
            	};
            	$.post('{{ route('voyager.orders.remove_product') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success('Товар удален из заказа');
                    	var id = '{{$dataTypeContent->id}}';
				    	if (id != 0)
				    		$(".table-striped").load("/admin/orders/{{$dataTypeContent->id}}/edit .table-striped > *");
				    	else
				    		$(".table-striped").load("/admin/orders/create .table-striped > *");
                        //$file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Ошибка удаления");
                    }
                });
            })

            $(document).on('click','#add_discount',function(){
            	var params = {
            		order_id: '{{$dataTypeContent->id}}',
            		discount_sum: $('#discount-sum').val(),
            		discount_percent: $('#discount-percent').val(),
            		_token: '{{ csrf_token() }}'
            	};
            	$.post('{{ route('voyager.orders.discount') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success('Данные изменены');
                    	var id = '{{$dataTypeContent->id}}';
				    	if (id != 0)
				    		$(".table-striped").load("/admin/orders/{{$dataTypeContent->id}}/edit .table-striped > *");
				    	else
				    		$(".table-striped").load("/admin/orders/create .table-striped > *");
                    } else {
                        toastr.error("Ошибка");
                    }
                });
            	return false;
            })

            $(document).on('click','#add_discount-live',function(){
            	var params = {
            		order_id: '{{$dataTypeContent->id}}',
            		discount_sum: $('#discount-sum').val(),
            		discount_percent: $('#discount-percent').val(),
            		_token: '{{ csrf_token() }}'
            	};
            	$.post('{{ route('voyager.orders.discount_new') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success('Данные изменены');
                    	var id = '{{$dataTypeContent->id}}';
				    	if (id != 0)
				    		$(".table-striped").load("/admin/orders/{{$dataTypeContent->id}}/edit .table-striped > *");
				    	else
				    		$(".table-striped").load("/admin/orders/create .table-striped > *");
                    } else {
                        toastr.error("Ошибка");
                    }
                });
            	return false;
            })

            $("#add-product-button").click(function(){
            	//modal-body
            	var params = {
            		order_id: '{{$dataTypeContent->id}}',
            		_token: '{{ csrf_token() }}'
            	};

            	$.ajax({
				  	type: 'POST',
				  	url: '{{ route('voyager.orders.products') }}',
				  	data: params,
				  	dataType: 'html',
				  	success: function(data){
				    	$('#product-list .modal-body').empty().html(data);
				    	$("#product-list").modal('show');
				  	}
				});
            	//$("#product-list").modal('show');
            	return false;
            })
            
            $("#confirm_product").click(function(){
            	var products = [];
            	$(".product-checkbox:checked").each(function(){
            		products.push($(this).val());
            	})
            	var params = {
            		order_id: '{{$dataTypeContent->id}}',
            		_token: '{{ csrf_token() }}',
            		products: products
            	};
            	$.ajax({
				  	type: 'POST',
				  	url: '{{ route('voyager.orders.edit-products') }}',
				  	data: params,
				  	dataType: 'html',
				  	success: function(data){
				  		toastr.success('Данные изменены');
				    	$("#product-list").modal('hide');
				    	var id = '{{$dataTypeContent->id}}';
				    	if (id != 0)
				    		$(".table-striped").load("/admin/orders/{{$dataTypeContent->id}}/edit .table-striped > *");
				    	else
				    		$(".table-striped").load("/admin/orders/create .table-striped > *");
				  	}
				});
            	return false;
            })
        });
    </script>
    <script type="text/javascript">
    	$(function(){
    		$(".datepickert").datetimepicker({"locale": "ru","format": "YYYY-MM-DD HH:mm"});

    		$(document).on('click','.product-tr', function(){
    			var checkbox = $(this).find('input[type="checkbox"]');
			    checkbox.prop('checked', !checkbox.prop('checked'));
			    if (checkbox.prop('checked') == true)
			    	$(this).addClass('selected-product');
			    else
			    	$(this).removeClass('selected-product');
    		});

    		/*$('.markerDiv').on('click', function(){
			    
			});*/
    	})
    </script>
@stop
