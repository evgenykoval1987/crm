<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped" id="dataTable">
    		<thead>
    			<tr>
    				<th>Услуга</th>
    				<th>Цена</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($items as $key => $item)
    				<tr>
    					<td colspan="4"><b style="text-decoration: underline;">{{$key}}</b></td>
    				</tr>
    				@foreach($item as $product)
    					<tr class="product-tr {{ (in_array($product->id, $order_products)) ? 'selected-product' : ''  }}" style="cursor: pointer;">
    						<td>&nbsp;{{$product->title}}</td>
    						<td>
                                {{$product->price}}
                                <div style="display: none;">
                                    @if(in_array($product->id, $order_products))
                                        <input type="checkbox" name="products[]" class="product-checkbox" value="{{$product->id}}" checked="checked">
                                    @else
                                        <input type="checkbox" name="products[]" class="product-checkbox" value="{{$product->id}}">
                                    @endif
                                </div>
                            </td>
    					</tr>
    				@endforeach
    			@endforeach
    		</tbody>
    	</table>
	</div>
</div>