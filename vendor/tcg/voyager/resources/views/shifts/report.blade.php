<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body {
	        height: 842px;
	        width: 595px;
	        padding: 10px 20px 10px 20px;
	        font-size: 14px;
	    }
	    #header{
	    	text-align: center;
	    	margin-bottom: 10px;
	    }
	    #subheader > div{
	    	display: inline-block;
	    	padding: 10px;
	    }
	    #subheader > div > div{
	    	padding: 5px;
	    }
	    #table > table{
	    	border: 1px solid #000;
	    	border-collapse: collapse;
	    	margin-top: 10px;
	    }
	    #table > table th{
	    	padding: 5px;
	    	border: 1px solid #000;
	    	background-color: #ccc;
	    }
	    #table > table td{
	    	padding: 5px;
	    	border: 1px solid #000;
	    }
	    @media print {
		    #table > table th{
		        background-color: #ccc;
		        -webkit-print-color-adjust: exact; 
		    }
		}
	</style>
</head>
<body>
	<div id="header"><h4>Бланк закрытия смены. (УД-001)</h4></div>
	<div id="subheader">
		<div style="width: 40%">
			<div>Дата: {{$date_begin}}</div>
			<div>Сотрудник: {{$courier}}</div>
		</div>
		<div style="width: 50%">
			<div>Начал смену: {{$time_begin}}</div>
			<div>Пробег на начало смены: __________</div>
		</div>
	</div>
	<div id="table">
		<table>
			<tr>
				<th style="width: 15%; background-color: #ccc;">Номер договора</th>
				<th style="width: 40%">Наименование услуги</th>
				<th style="width: 15%">Сумма к оплате</th>
				<th style="width: 15%">Заявку открыл, время:</th>
				<th style="width: 15%">Заявку выполнил, время:</th>
			</tr>
			@foreach($orders as $order)
				<tr>
					<td>{{$order['order_id']}}</td>
					<td>
						@foreach($order['services'] as $service)
							{{$service}}<br>
						@endforeach
					</td>
					<td>{{$order['total']}} р.</td>
					<td>{{date('H:i', strtotime($order['start_dt']))}}</td>
					<td>{{date('H:i', strtotime($order['finish_dt']))}}</td>
				</tr>
			@endforeach
			@if($dop != 0)
				<tr>
					<td colspan="3"></td>
					<td><b>Стоимость:</b></td>
					<td><b>{{$total}} р.</b></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td><b>Дополнительные расходы:</b></td>
					<td><b>{{$dop}} р.</b></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td><b>Итого:</b></td>
					<td><b>{{$total - $dop}} р.</b></td>
				</tr>
				<tr>
					<td colspan="4">
						<b>Дополнительные расходы:</b>
						@foreach ($shift->shiftcosts as $cost)
							{{$cost->text}} |
						@endforeach
					</td>
				</tr>
			@else
				<tr>
					<td colspan="3"></td>
					<td><b>Итого:</b></td>
					<td><b>{{$total}} р.</b></td>
				</tr>
			@endif
		</table>
		<table style="width: 100%">
			<tr>
				<th colspan="3">Количество заявок:</th>
			</tr>
			<tr>
				<td style="text-align: center;">Кремация</td>
				<td style="text-align: center;">Эвтаназия</td>
			</tr>
			<tr>
				<td style="text-align: center;">{{$crem}}</td>
				<td style="text-align: center;">{{$evt}}</td>
			</tr>
		</table>
	</div>
	<div id="subheader">
		<div style="width: 40%">
			<div>Завершил смену: {{$date_end}}</div>
		</div>
		<div style="width: 50%">
			<div>Пробег на конец смены: ______________</div>
		</div>
	</div>
	<div style="margin-top: 10px">
		<h3 style="text-align: right;">ДС принял: ____________________ / _________</h3>
	</div>
</body>
<script type="text/javascript">
	window.print();
</script>
</html>