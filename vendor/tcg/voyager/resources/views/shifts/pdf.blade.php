<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		/*body {
	        height: 842px;
	        width: 595px;
	        padding: 10px 20px 10px 20px;
	        font-size: 14px;
	    }
	    /*#header{
	    	text-align: center;
	    	margin-bottom: 10px;
	    }
	    /*#subheader > div{
	    	display: inline-block;
	    	padding: 10px;
	    }
	    #subheader > div > div{
	    	padding: 5px;
	    }*/
	    /*#table > table{
	    	border: 1px solid #000;
	    	border-collapse: collapse;
	    	margin-top: 10px;
	    }*/
	    /*#table > table tr th{
	    	padding: 5px;
	    	border: 1px solid #000;
	    	background-color: #ccc;
	    }*/
	    /*#table > table td{
	    	padding: 5px;
	    	border: 1px solid #000;
	    }*/
	    @media print {
		    #table > table th{
		        background-color: #ccc;
		        -webkit-print-color-adjust: exact; 
		    }
		}
	</style>
</head>
<body>
	<div id="header" style="text-align: center; margin-bottom: 10px;"><h4>Бланк закрытия смены. (УД-001)</h4></div>
	<div id="subheader">
		<div style="width: 43%; padding: 10px; float: left;">
			<div style="padding: 5px;">Дата: {{$date_begin}}</div>
			<div style="padding: 5px;">Сотрудник: {{$courier}}</div>
		</div>
		<div style="width: 50%; padding: 10px; float: right; right: 0px; text-align: right;">
			<div style="padding: 5px;">Начал смену: {{$time_begin}}</div>
			<div style="padding: 5px;">Пробег на начало смены: __________</div>
		</div>
	</div>
	<div id="table">
		<table style="border: 1px solid #000; border-collapse: collapse; margin-top: 10px;">
			<tr>
				<th style="width: 15%; padding: 5px; border: 1px solid #000; background-color: #ccc;">Номер договора</th>
				<th style="width: 40%; padding: 5px; border: 1px solid #000; background-color: #ccc;">Наименование услуги</th>
				<th style="width: 15%; padding: 5px; border: 1px solid #000; background-color: #ccc;">Сумма к оплате</th>
				<th style="width: 15%; padding: 5px; border: 1px solid #000; background-color: #ccc;">Заявку открыл, время:</th>
				<th style="width: 15%; padding: 5px; border: 1px solid #000; background-color: #ccc;">Заявку выполнил, время:</th>
			</tr>
			@foreach($orders as $order)
				<tr>
					<td style="padding: 5px;border: 1px solid #000;">{{$order['order_id']}}</td>
					<td style="padding: 5px;border: 1px solid #000;">
						@foreach($order['services'] as $service)
							{{$service}}<br>
						@endforeach
					</td>
					<td style="padding: 5px;border: 1px solid #000;">{{$order['total']}} р.</td>
					<td style="padding: 5px;border: 1px solid #000;">{{date('H:i', strtotime($order['start_dt']))}}</td>
					<td style="padding: 5px;border: 1px solid #000;">{{date('H:i', strtotime($order['finish_dt']))}}</td>
				</tr>
			@endforeach
			@if($dop != 0)
				<tr>
					<td colspan="3" style="padding: 5px;border: 1px solid #000;"></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>Стоимость:</b></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>{{$total}} р.</b></td>
				</tr>
				<tr>
					<td colspan="3" style="padding: 5px;border: 1px solid #000;"></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>Дополнительные расходы:</b></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>{{$dop}} р.</b></td>
				</tr>
				<tr>
					<td colspan="3" style="padding: 5px;border: 1px solid #000;"></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>Итого:</b></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>{{$total - $dop}} р.</b></td>
				</tr>
				<tr>
					<td colspan="4" style="padding: 5px;border: 1px solid #000;">
						<b>Дополнительные расходы:</b>
						@foreach ($shift->shiftcosts as $cost)
							{{$cost->text}} |
						@endforeach
					</td>
				</tr>
			@else
				<tr>
					<td colspan="3" style="padding: 5px;border: 1px solid #000;"></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>Итого:</b></td>
					<td style="padding: 5px;border: 1px solid #000;"><b>{{$total}} р.</b></td>
				</tr>
			@endif
		</table>
		<table style="width: 100%; margin-top: 50px;border: 1px solid #000; border-collapse: collapse; margin-top: 10px;">
			<tr>
				<th colspan="2" style="padding: 5px; border: 1px solid #000; background-color: #ccc;">Количество заявок:</th>
			</tr>
			<tr>
				<td style="text-align: center;padding: 5px;border: 1px solid #000;">Кремация</td>
				<td style="text-align: center;padding: 5px;border: 1px solid #000;">Эвтаназия</td>
			</tr>
			<tr>
				<td style="text-align: center;padding: 5px;border: 1px solid #000;">{{$crem}}</td>
				<td style="text-align: center;padding: 5px;border: 1px solid #000;">{{$evt}}</td>
			</tr>
		</table>
	</div>
	<div id="subheader">
		<div style="width: 40%; float: left">
			<div>Завершил смену: {{$date_end}}</div>
		</div>
		<div style="width: 50%; float: right; text-align: right;">
			<div>Пробег на конец смены: ______________</div>
		</div>
	</div>
	<div style="margin-top: 10px">
		<h3 style="text-align: right;">ДС принял: ____________________ / _________</h3>
	</div>
</body>
<script type="text/javascript">
	window.print();
</script>
</html>