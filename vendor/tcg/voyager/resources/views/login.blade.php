<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="admin login">
    <title>{{ Voyager::setting("admin.title") }}</title>
    <link rel="stylesheet" href="{{ voyager_asset('app/css/app.css') }}">
    <style type="text/css">
        .text-danger{
            background: #e74c3c;
            border-left: 5px solid rgba(0,0,0,.1);
            font-size: 12px;
            color: #fff;
            position: relative;
            z-index: 10;
            margin-top: 20px;
            padding-right: 20px;
            padding-left: 20px;
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            display: none;
        }
    </style>
    <script>
        function canUseWebP(){var e=document.createElement("canvas");return!(!e.getContext||!e.getContext("2d"))&&0==e.toDataURL("image/webp").indexOf("data:image/webp")}var root=document.getElementsByTagName("html")[0];canUseWebP()?root.classList.add("ws"):root.classList.add("wn");
    </script>
</head>
<body>
    <main class="container">
        <div class="container__layout container__layout_fixed">
            <header class="header">
                <div class="header__wrap wrap wrap_big">
                    <div class="header__row flex flex_justify flex_vertical">
                        <div class="header__cell">
                            <a class="header__logo logo" href="/">
                                <picture>
                                    <source type="image/webp" srcset="{{ voyager_asset('app/img/logo.webp') }}">
                                    <img src="{{ voyager_asset('app/img/logo.png') }}" alt="" role="presentation" />
                                </picture>
                            </a>
                        </div>
                        <div class="header__cell flex flex_vertical">
                            <a class="header__phone phone" href="#">
                                <picture>
                                    <source type="image/webp" srcset="{{ voyager_asset('app/img/phone.webp') }}">
                                    <img src="{{ voyager_asset('app/img/phone.png') }}" class="phone__icon" alt="" role="presentation" />
                                </picture>8 (495) 123-45-67</a>
                            <div class="header__text">(Круглосуточный колл-центр)</div>
                        </div>
                        <div class="header__cell">
                            <a class="header__login login js-modal" href="#login">
                                <picture>
                                    <source type="image/webp" srcset="{{ voyager_asset('app/img/login.webp') }}">
                                    <img src="{{ voyager_asset('app/img/login.png') }}" class="login__icon" alt="" role="presentation" />
                                </picture>Вход <span>для&nbsp;партнёров</span>
                            </a>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="container__layout container__layout_fluid">
            <section class="section">
                <div class="section__wrap wrap">
                    <div class="section__top">Система комплексного обслуживания для&nbsp;ветеринарных клиник и&nbsp;кабинетов</div>
                    <div class="section__row flex flex_justify">
                        <div class="section__item">
                            <div class="section__head flex">
                                <div class="section__head-cell section__head-cell_icon">
                                    <picture>
                                        <source type="image/webp" srcset="{{ voyager_asset('app/img/car.webp') }}">
                                            <img src="{{ voyager_asset('app/img/car.png') }}" alt="" role="presentation" />
                                    </picture>
                                </div>
                                <div class="section__head-cell section__head-cell_text">Вывоз
                                    <br>и&nbsp;утилизация</div>
                            </div>
                            <div class="section__preview">
                                <div class="section__preview-image">
                                    <picture>
                                        <source type="image/webp" srcset="{{ voyager_asset('app/img/item_1.webp') }}">
                                            <img src="{{ voyager_asset('app/img/item_1.jpg') }}" class="object-fit" alt="" role="presentation" />
                                    </picture>
                                </div>
                                <div class="section__inner flex flex_justify">
                                    <div class="section__part">
                                        <div class="section__border">медицинские отходы</div><a class="section__small" href="#">Проект договора</a>
                                    </div>
                                    <div class="section__part">
                                        <div class="section__border">биологические отходы</div><a class="section__small" href="#">Проект договора</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section__item">
                            <div class="section__head flex">
                                <div class="section__head-cell section__head-cell_icon">
                                    <picture>
                                        <source type="image/webp" srcset="{{ voyager_asset('app/img/dog.webp') }}">
                                            <img src="{{ voyager_asset('app/img/dog.png') }}" alt="" role="presentation" />
                                    </picture>
                                </div>
                                <div class="section__head-cell section__head-cell_text">Оказание ритуальных услуг
                                    <br>полного цикла для&nbsp;домашних животных</div>
                            </div>
                            <div class="section__preview">
                                <div class="section__preview-image">
                                    <picture>
                                        <source type="image/webp" srcset="{{ voyager_asset('app/img/item_2.webp') }}">
                                            <img src="{{ voyager_asset('app/img/item_2.jpg') }}" class="object-fit" alt="" role="presentation" />
                                    </picture>
                                </div>
                                <div class="section__inner flex flex_right">
                                    <div class="section__part">
                                        <div class="section__border section__border_right">Вывоз из&nbsp;клиники или с адреса вашего клиента до&nbsp;2-х часов</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section__bottom css-text-center">
                        <a class="section__button btn" href="#"> <span class="btn__text">КОМЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ</span>
                        </a>
                    </div>
                </div>
            </section>
        </div>
        <div class="container__layout container__layout_fixed">
            <footer class="footer">
                <div class="footer__wrap wrap wrap_big">
                    <div class="footer__row flex flex_justify flex_vertical">
                        <div class="footer__cell"><a class="footer__link" href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__cell">
                            <a class="footer__logo" href="/">
                                <picture>
                                    <source type="image/webp" srcset="{{ voyager_asset('app/img/logo_footer.webp') }}">
                                        <img src="{{ voyager_asset('app/img/logo_footer.png') }}" alt="" role="presentation" />
                                </picture>
                            </a>
                        </div>
                        <div class="footer__cell">
                            <div class="footer__text">ООО “Эковет” ИНН 1122334455 ОГРН 1122334455
                                <br>г. Москва, Головинское шоссе, д. 5, БЦ Водный</div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </main>
    <div class="modals mfp-hide" id="login">
        <div class="modals__top flex flex_vertical flex_center">Войти в&nbsp;кабинет</div>
        <div class="modals__bottom">
            <form class="modals__form form" action="{{ route('voyager.login') }}" id="login-form">
                {{ csrf_field() }}
                <div class="form__field">
                    <input class="form__input" name="email" type="text" placeholder="Введите Email" />
                </div>
                <div class="form__field">
                    <input class="form__input" name="password" type="password" placeholder="Введите пароль" />
                </div>
                <div class="form__field">
                    <button class="form__button btn" type="submit">
                        <div class="btn__text">Войти</div>
                    </button>
                </div>
                <div class="text-danger">Эти учетные данные не соответствуют нашим записям.</div>
            </form><a class="modals__clear" href="#">Сбросить пароль</a>
            <div class="modals__text">Пароль будет сброшен после проверки администратором</div>
        </div>
    </div>
    <script src="{{ voyager_asset('app/js/app.js') }}"></script>
    <script type="text/javascript">
        $(function(){
            $("#login-form").submit(function(){
                $(".text-danger").hide();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('voyager.login') }}',
                    data: $("#login-form input"),
                    success: function (json){
                        if (json.success == true){
                            document.location.reload();
                        }
                        else{
                            $(".text-danger").show();
                        }
                    },
                    error: function(){
                        $(".text-danger").show();
                    }
                });
                return false;
            })
        })
    </script>

</body>
</html>
