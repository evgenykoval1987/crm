<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use TCG\Voyager\Facades\Voyager;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Client;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;
use TCG\Voyager\Models\User;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\Product;
use TCG\Voyager\Models\UserLog;
use TCG\Voyager\Models\Shift;
use TCG\Voyager\Models\Time;
use TCG\Voyager\Models\ShiftCost;

class VoyagerTelegramController extends Controller{

    public function index(Request $request){
    	$token = "1032235453:AAE5SjViDDIqIPRIctg6szrwxeb4LJwYMUo";
        $bot = new Client($token);

        $self = $this;
        
        $bot->callbackQuery(function ($message) use ($bot, $self) {
            $input = $message->getMessage()->getText();
            $cid = $message->getFrom()->getId();

            $command = $message->getData();

            $self->order($message->getMessage(), $bot, $command, $message->getFrom()->getUsername());
        });

        $bot->command('start', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();
            $firstname = $message->getFrom()->getFirstName();
            $lastname = $message->getFrom()->getLastName();

            if ($self->checkCourier($user)){
                $answer = "Добро пожаловать $firstname $lastname!";
            }
            else{
                $answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            }

            $bot->sendMessage($message->getChat()->getId(), $answer);
            
            $user = User::where('telegram',$user)->first();
            $user->telegram_chat_id = $message->getChat()->getId();
            $user->save();
        });

        $bot->command('begin', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();

            if ($self->checkCourier($user)){
            	$status = $self->courierStatus($user);
            	if ($status == '3'){
            		$answer = "Лимит открытых смен! На данный момент смены открыты у других ветеринаров. Вы сможете открыть свою смену после того как закроется смена одна из смен.";
            		$bot->sendMessage($message->getChat()->getId(), $answer);
            	}
            	if ($status == '2'){
            		$answer = "Ваша смена уже открыта.";
            		$bot->sendMessage($message->getChat()->getId(), $answer);
            	}
            	if ($status == '1'){
            		$user_info = User::where('telegram', $user)->first();

            		$answer = "Вы открыли свою смену.";
            		$bot->sendMessage($message->getChat()->getId(), $answer);

                    $courier = User::where('telegram',$user)->first();
                    $courier->shift_status = 1;
                    $courier->save();

                    $log = new UserLog();
                    $log->type = '1';
                    $log->user_id = $user_info->id;
                    $log->text = "Ветеринар $courier->name открыл смену - ".date('d.m.Y H:i').".";
                    $log->save();

                    $shift = new Shift();
                    $shift->user_id = $user_info->id;
                    $shift->begin_dt = now();
                    $shift->status = 1;
                    $shift->save();

                    $orders = Order::where('telegram_status','new')->get();
                    foreach ($orders as $key => $order) {
                    	$self->sendOrder($order->id);
                    }
            	}
            }
            else{
            	$answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            	$bot->sendMessage($message->getChat()->getId(), $answer);
            }
        });

		$bot->command('end', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();
            if ($self->checkCourier($user)){
            	$user_info = User::where('telegram', $user)->first();
            	if ($user_info->shift_status != '1'){
            		$answer = 'У вас не открыта смена.';
            		$bot->sendMessage($message->getChat()->getId(), $answer);
            	}
            	else{
            		$flag = true;

            		$orders = Order::where('vet_id',$user_info->id)->where('telegram_status','process')->get();
                    if ($orders->count() > 0){
                    	$flag = false;
                        $str = [];
                        foreach ($orders as $key => $order) {
                            $str[] = $order->id;
                        }
                        $str = implode(',', $str);
                        $answer = 'Вы закрыли не все заказы. Закройте заказы чтобы завершить смену. Заказы - '.$str;
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                        exit;
                    }

                    $shift = $self->getCurrentShift($user_info->id);
                    if ($shift){
                        $sum = trim(str_replace('/end', '', $message->getText()));
                        if ($sum == ''){
                            $answer = "Смена не закрыта. Укажите сумму на руках /end <сумма>";
                            $bot->sendMessage($message->getChat()->getId(), $answer);
                        	exit;
                        }
                        else{
                            $total = 0;
                            if ($shift->orders){
	                            foreach ($shift->orders as $order){
	                                $total += $order->total($order->id);
	                            }

	                            $dop = 0;
	                            foreach ($shift->shiftcosts as $cost){
	                                $dop += (int)$cost->total;
	                            }

	                            $total -= $dop;

	                            if ($sum != $total){
	                                $answer = "Смена не закрыта. Внесенная сумма на руках не соответсвует введенным данным. Сумма в системе по смене - $total р.";
	                                $bot->sendMessage($message->getChat()->getId(), $answer);
	                        		exit;
	                            }
	                        }
                        }
                    }

            		if ($flag){
                        $answer = 'Вы закрыли свою смену.';
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                        $user_info->shift_status = 0;
                        $user_info->save();
                        
                        $log = UserLog::where('user_id', $user_info->id)->latest()->first();
                        $opened = date('d.m.Y H:i', strtotime($log->created_at));
                        $closed = date('d.m.Y H:i');
                        $ts1 = strtotime($log->created_at);
                        $ts2 = strtotime(date('d.m.Y H:i'));
                        $diff = $ts2 - $ts1;
                        $hours = date('H', $diff);
                        $minuts = date('i', $diff);
                        
                        $log = new UserLog();
                        $log->type = '2';
                        $log->user_id = $user_info->id;
                        $log->text = "Ветеринар $user_info->name закрыл смену. Смена открыта - $opened. Смена закрыта - $closed. Отработано времени - $hours часов $minuts минут";
                        $log->save();

                        if ($shift){
	                        $shift->end_dt = now();
	                        $shift->status = 0;
	                        $shift->save();
	                    }
                    }
            	}
            }
            else{
            	$answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            	$bot->sendMessage($message->getChat()->getId(), $answer);
            }
            /*if ($self->checkCourier($user)){
                $flag = $self->courier($user);
                if ($flag == 1 || $flag == 3){
                    $answer = 'У вас не открыта смена.'.$flag;
                }
                else{
                    $cur = Courier::where('telegramuser',$user)->first();

                    $orders = Order::where('courier_id',$cur->id)->where('status','start')->get();
                    if ($orders->count() > 0){
                        $str = [];
                        foreach ($orders as $key => $order) {
                            $str[] = $order->number;
                        }
                        $str = implode(',', $str);
                        $answer = 'Вы закрыли не все заказы. Закройте заказы чтобы завершить смену. Заказы - '.$str;
                    }
                    else{
                        $flag = true;
                        $shift = $self->getCurrentShift($cur->id);
                        if ($shift){
                            $sum = trim(str_replace('/end', '', $message->getText()));
                            if ($sum == ''){
                                $answer = "Смена не закрыта. Укажите сумму на руках /end <сумма>";
                                $flag = false;
                            }
                            else{
                                $total = 0;
                                foreach ($shift->orders as $order){
                                    $total += $order->total;
                                }

                                $dop = 0;
                                foreach ($shift->shiftcosts as $cost){
                                    $dop += (int)$cost->text;
                                }

                                $total -= $dop;

                                if ($sum != $total){
                                    $answer = "Смена не закрыта. Внесенная сумма на руках не соответсвует введенным данным. Сумма в системе по смене - $total р.";
                                    $flag = false;
                                }
                            }
                            
                            if ($flag) {
                                $shift->end_dt = now();
                                $shift->status = 0;
                                $shift->save();
                            }
                        }

                        if ($flag){
                            $answer = 'Вы закрыли свою смену.';
                            Courier::where('status', 1)->update(['status' => 0]);

                            $log = Log::latest()->first();
                            $opened = date('d.m.Y H:i', strtotime($log->created_at));
                            $closed = date('d.m.Y H:i');
                            $ts1 = strtotime($log->created_at);
                            $ts2 = strtotime(date('d.m.Y H:i'));
                            $diff = $ts2 - $ts1;
                            $hours = date('H', $diff);
                            $minuts = date('i', $diff);
                            
                            $log = new Log();
                            $log->type = '0';
                            $log->text = "Ветеринар $cur->name закрыл смену. Смена открыта - $opened. Смена закрыта - $closed. Отработано времени - $hours часов $minuts минут";
                            $log->save();
                        }
                    }
                }
            }
            else{
                $answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            }
            $bot->sendMessage($message->getChat()->getId(), $answer);*/
        });

		$bot->command('order', function ($message) use ($bot, $self ) {
            /*$answer = "1";
            $bot->sendMessage($message->getChat()->getId(), $answer);
            exit;*/
            $self->order($message, $bot);
        });

        $bot->command('cancel', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();
            if ($self->checkCourier($user)){
            	$user_info = User::where('telegram', $user)->first();
	            $order_id = trim(str_replace('/cancel', '', $message->getText()));
	                
	            $arr = explode(' ', $order_id);
	            $order_id = $arr[0];
	            if (isset($arr[1])){
	                unset($arr[0]);
	                $comment = implode(' ', $arr);
	            }
	            else
	                $comment = '';

	            $order_info = Order::where('id',$order_id)->first();
	            if ($order_info->vet_id){
	            	if ($order_info->vet_id != $user_info->id){
	            		$answer = "Вы не можете отменить заказ. Он в работе у другого ветеринара.";
            			$bot->sendMessage($message->getChat()->getId(), $answer);
            			exit;
	            	}
	            }
	            if ($order_info){
	                $order_info->telegram_status = 'close';
	                $order_info->status_id = 3;
	                $order_info->pay = 0;
	                $order_info->topay = 0;
	                $order_info->vcomment = $comment;
	                $order_info->close_dt = now();
	                $order_info->save();
	            }

	            $answer = "Заказ #$order_id успешно отменен.";
	            $bot->sendMessage($message->getChat()->getId(), $answer);
	            exit;
	        }
	        else{
	        	$answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            	$bot->sendMessage($message->getChat()->getId(), $answer);
            	exit;
	        }
        });

        $bot->command('close', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();
            if ($self->checkCourier($user)){
            	$user_info = User::where('telegram', $user)->first();

                $order_id = trim(str_replace('/close', '', $message->getText()));
                
                $arr = explode(' ', $order_id);
                $order_id = $arr[0];
                if (isset($arr[1])){
                    $total = $arr[1];
                    $order_info = Order::where('id',$order_id)->first();

                    if ($order_info->total($order_info->id) != $total){
                    	$answer = "Внесенная сумма не совпадает с полученной. Свяжитесь с администратором для коррекции заказа";
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                        exit;
                    }

                    if ($order_info->telegram_status != 'process'){
                        $answer = "Вы не можете закрыть не принятый заказ";
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                        exit;
                    }

                    if ($order_info->vet_id != $user_info->id){
                    	$answer = "Вы не можете закрыть заказ. Он в работе у другого ветеринара.";
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                        exit;
                    }

	                $status_id = 7;

	                /*$products = explode(',', $order->services);
	                foreach ($products as $key => $product) {
	                    $product = trim($product);
	                    if ($product == '') continue;
	                    if (in_array($product, ['500','40713','40714','4071','4072','4073','4074','4075'])){
	                        $status = 'waitforvideo';
	                        $answer = "Статус закрытого заказ #".$order_id." Ждет видеоотчет";
	                    }
	                    if (in_array($product, ['40013', '40014', '4001', '4002', '4003', '4004', '4005', '4006']) && $status != 'waitforvideo'){
	                        $status = 'waitforurna';
	                        $answer = "Статус закрытого заказ #".$order_id." Ждет доставки праха";
	                    }
	                    if (in_array($product, ['800']) && $status != 'waitforvideo'){
	                        $status = 'send-to-assembling';
	                        $answer = "Статус закрытого заказ #".$order_id." Урна на хранении";
	                    }
	                    if (in_array($product, ['1100', '1200', '1300']) && $status != 'waitforvideo'){
	                        $status = 'wait-for-dog';
	                        $answer = "Статус закрытого заказ #".$order_id." Ждет доставки тела";
	                    }
	                }*/

	                $order_info->telegram_status = 'close';
	                $order_info->close_dt = now();
	                $order_info->status_id = $status_id;
	                $order_info->save();

	                $answer = "Вы закрыли заказ #".$order_id;
	                $bot->sendMessage($message->getChat()->getId(), $answer);
                }
                else{
                    $answer = "Укажите сумму, полученную с заказа в формате /close <номер заказа> <сумма>";
                    $bot->sendMessage($message->getChat()->getId(), $answer);
                    exit;
                }
            }
            else{
                $answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
                $bot->sendMessage($message->getChat()->getId(), $answer);
            }
        });

        $bot->command('spend', function ($message) use ($bot, $self) {
            $user = $message->getFrom()->getUsername();
            if ($self->checkCourier($user)){
            	$status = $self->courierStatus($user);
        		if ($status == '2'){
	                $text = trim(str_replace('/spend', '', $message->getText()));

	                $arr = explode(' ', $text);
	                $text = (isset($arr[0])) ? $arr[0] : '';
	                $total = (isset($arr[1])) ? $arr[1] : '0';

	                $user_info = User::where('telegram', $user)->first();
	                $shift = $self->getCurrentShift($user_info->id);
	                if ($shift){
	                   $shiftcost = new ShiftCost();
	                   $shiftcost->shift_id = $shift->id;
	                   $shiftcost->text = $text;
	                   $shiftcost->total = $total;
	                   $shiftcost->save();
	                   $answer = "Расход '$text' добавлен в ведомость смены";
	                   $bot->sendMessage($message->getChat()->getId(), $answer);
	                   exit;
	                }
	                else{
	                	$answer = "У вас не открыта смена. 111";
	                	$bot->sendMessage($message->getChat()->getId(), $answer);
	                	exit;
	                }
	            }
	            else{
	            	$answer = "У вас не открыта смена.";
                	$bot->sendMessage($message->getChat()->getId(), $answer);
                	exit;
	            }
            }
            else{
                $answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
                $bot->sendMessage($message->getChat()->getId(), $answer);
                exit;
            }
        });
        
        $bot->run();
    }

    public function checkCourier($telegram_user){
    	$user = User::where('telegram', $telegram_user)->get()->count();
    	if ($user > 0)
    		return true;
    	else
    		return false;
    }

    // 1 - Открываем смену
    // 2 - Уже открыта
    // 3 - Лимит
    public function courierStatus($telegram_user){
    	$user = User::where('telegram', $telegram_user)->first();

    	$opened = User::where('shift_status', '1')->get()->count();
    	if ($opened > 1){
    		return '3';
    	}

    	if ($user->shift_status == '1'){
    		return '2';
    	}
    	else{
    		return '1';
    	}
    }

    public function sendOrder($order_id){
    	$token = "1032235453:AAE5SjViDDIqIPRIctg6szrwxeb4LJwYMUo";

    	$order_info = Order::findOrFail($order_id);
    	if ($order_info->status_id == 2){
    		$address = [];
    		if ($order_info->address_city != '')
    			$address[] = 'г. '.$order_info->address_city;
    		if ($order_info->address_street != '')
    			$address[] = 'ул. '.$order_info->address_street;
    		if ($order_info->address_house != '')
    			$address[] = 'д. '.$order_info->address_house;
    		if ($order_info->address_corpus != '')
    			$address[] = 'к. '.$order_info->address_corpus;
    		if ($order_info->address_porch != '')
    			$address[] = 'подьезд '.$order_info->address_porch;
    		if ($order_info->address_floor != '')
    			$address[] = 'этаж '.$order_info->address_floor;
    		if ($order_info->address_intercom != '')
    			$address[] = 'домофон '.$order_info->address_intercom;
    		$address = implode(',', $address);

    		$products = [];
    		foreach ($order_info->products as $key => $product) {
    			$products[] = $product->product->title.' - '.(int)$product->product->price.' руб.';
    		}
    		$products = implode(';', $products);

    		$users = User::where('shift_status','1')->get();
    		foreach ($users as $key => $user) {
                $bot = new Client($token);
                $message = "НОВЫЙ ЗАКАЗ
    Номер заказа - $order_info->id 
    Имя - $order_info->client 
    Телефон - $order_info->telephone 
    Адрес - $address 
    Комментарий менеджера - $order_info->ocomment
    Услуги - $products
    Общая - ".$order_info->total($order_info->id)." руб.";
                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Принять заказ #'.$order_info->id, 'callback_data' => '/order '.$order_info->id]
                        ]
                    ]
                );

                $bot->sendMessage($user->telegram_chat_id, $message, null, false, null, $keyboard);
    		}
    	}
    	elseif ($order_info->status_id == 4){
    		$dt = date('d.m.Y', strtotime($order_info->deffered_dt));
    		$time = '';
    		if ($order_info->time_id){
    			$time = Time::find($order_info->time_id);
    			$time = $time->title;
    		}

    		$address = [];
    		if ($order_info->address_city != '')
    			$address[] = 'г. '.$order_info->address_city;
    		if ($order_info->address_street != '')
    			$address[] = 'ул. '.$order_info->address_street;
    		if ($order_info->address_house != '')
    			$address[] = 'д. '.$order_info->address_house;
    		if ($order_info->address_corpus != '')
    			$address[] = 'к. '.$order_info->address_corpus;
    		if ($order_info->address_porch != '')
    			$address[] = 'подьезд '.$order_info->address_porch;
    		if ($order_info->address_floor != '')
    			$address[] = 'этаж '.$order_info->address_floor;
    		if ($order_info->address_intercom != '')
    			$address[] = 'домофон '.$order_info->address_intercom;
    		$address = implode(',', $address);

    		$products = [];
    		foreach ($order_info->products as $key => $product) {
    			$products[] = $product->product->title.' - '.(int)$product->product->price.' руб.';
    		}
    		$products = implode(';', $products);

    		$users = User::where('shift_status',1)->get();
    		foreach ($users as $key => $user) {
    			$bot = new Client($token);
                $message = "ОТЛОЖЕННЫЙ ЗАКАЗ на $dt в $time
Номер заказа - $order_info->id 
Имя - $order_info->client 
Телефон - $order_info->telephone 
Адрес - $address 
Комментарий менеджера - $order_info->ocomment
Услуги - $products
Общая - ".$order_info->total($order_info->id)." руб.";
                
                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Принять заказ #'.$order_info->id, 'callback_data' => '/order '.$order_info->id]
                        ]
                    ]
                );

                $bot->sendMessage($user->telegram_chat_id, $message, null, false, null, $keyboard);
    		}
    	}
    }

    public function order($message, $bot, $text = false, $user = false){
        if (!$user)
            $user = $message->getFrom()->getUsername();
        if ($this->checkCourier($user)){
        	$status = $this->courierStatus($user);
        	if ($status == '2'){
                if (!$text)
                    $order_id = trim(str_replace('/order', '', $message->getText()));
                else
                    $order_id = trim(str_replace('/order', '', $text));
        		$orders = explode(',', $order_id);


	            foreach ($orders as $key => $order_id) {
                    //file_put_contents('444', '111');
	            	$user_info = User::where('telegram',$user)->first();
	            	$order_info = Order::where('id',$order_id)->first();

                    if ($order_info){
    	            	if ($order_info->telegram_status != 'new' && $order_info->vet_id != $user_info->id){
    	            		$answer = "Заказ #$order_id недоступен или принят другим ветеринаром";
                        	$bot->sendMessage($message->getChat()->getId(), $answer);
                        	exit;
    	            	}

    	            	if ($order_info->telegram_status != 'new' && $order_info->vet_id == $user_info->id){
    	            		$answer = "Заказ #$order_id уже принят вами в работу";
                        	$bot->sendMessage($message->getChat()->getId(), $answer);
                        	exit;
    	            	}

                        $order_info->telegram_status = 'process';
                        $order_info->vet_id = $user_info->id;
                        $order_info->status_id = 5;
                        $order_info->receive_dt = now();
                         
                        $shift = $this->getCurrentShift($user_info->id);
                        if ($shift){
                           $order_info->shift_id = $shift->id;
                        }

                        $order_info->save();

                        $answer = "Вы приняли в работу заказ #".$order_id;
                    	$bot->sendMessage($message->getChat()->getId(), $answer);
                    }
                    else{
                        $answer = "Заказа не существует #".$order_id;
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                    }
	            }
        	}
        }
        else{
            $answer = "Ветеринара с вашими данными не существует. Логин Telegram не найден.";
            $bot->sendMessage($message->getChat()->getId(), $answer);
        }

        exit;
    }

    public function getCurrentShift($user_id){
        return Shift::where('user_id', $user_id)->where('status',1)->first();
    }
}