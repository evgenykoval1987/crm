<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\Column;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Database\Schema\Table;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\Product;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\OrderLog;
use TCG\Voyager\Models\OrderProduct;
use Illuminate\Validation\Rule;
use Session;
use Validator;

class VoyagerOrderController extends VoyagerBaseController
{
    /*public function profile(Request $request)
    {
        return Voyager::view('voyager::profile');
    }*/

    public function index(Request $request){
        Session::forget('order_products');
        Session::forget('order_discount');
        return parent::index($request);
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    { 
        $telephone = implode('|', $request->telephones);
        $request->merge([
            'telephone'                              => $telephone
        ]);

        $info = Order::find($id);
        if ($info->status_id != $request->status_id){
            $log = new OrderLog();
            $log->order_id = $id;
            $log->from_status_id = $info->status_id;
            $log->to_status_id = $request->status_id;
            $log->save();

            if ($request->status_id == 2 || $request->status_id == 4){ 
                $request->request->add(['telegram_status' => 'new']);
            }
            else{
                $request->request->add(['telegram_status' => '']);
            }

           
        }
       

        return parent::update($request, $id); 
    }

    public function store(Request $request){   

        $telephone = implode('|', $request->telephones);
        $request->merge([
            'telephone'                              => $telephone
        ]);
        if ($request->status_id == 2 || $request->status_id == 4){ 
            $request->request->add(['telegram_status' => 'new']);
        }
        
        /*$rules = [];
        $messages = [];
        $customAttributes = [];

        if (isset($request->productss)){
            if (!in_array('3', $request->productss)){
                $rules['productss'] = [
                    'required',
                    Rule::in(['3']),
                ];
                $customAttributes['productss'] = 'Состав заказа';
                $messages['in'] = 'Не выбран товар из категории "Услуги по выезду"';
            }
        }
        else{
            $rules['productss'] = [
                'required',
                Rule::in(['3']),
            ];
            $customAttributes['productss'] = 'Состав заказа';
            $messages['in'] = 'Не выбран товар из категории "Услуги по выезду"';
        }

        Validator::make($request->all(), $rules, $messages, $customAttributes);*/

        return parent::store($request); 
    }

    public function edit(Request $request, $id){
        //die();
        return parent::edit($request, $id); 
    }

    public function create(Request $request){
        /*Session::forget('order_products');
        Session::forget('order_discount');*/
        //dd($request->status_id);
        return parent::create($request); 
    }

    public function remove_product(Request $request){
        $order_id = $request->order_id;
        $product_id = $request->product_id;
        if ($order_id){
            OrderProduct::where('order_id', $order_id)->where('product_id', $product_id)->delete();
        }
        else{
            $prs = Session::get('order_products');

            foreach ($prs as $key => $pr) {
                if ($pr == $product_id){
                    unset($prs[$key]);
                }
            }
            Session::put('order_products', $prs);
        }

        return response()->json([
           'data' => [
               'status'  => 200,
               'message' => 1,
           ],
        ]);
    }

    public function discount(Request $request){
        $order_id = $request->order_id;
        $discount_sum = $request->discount_sum;
        $discount_percent = $request->discount_percent;

        $order = Order::findOrFail($order_id);
        $order->discount_sum = $discount_sum;
        $order->discount_percent = $discount_percent;
        $order->save();

        return response()->json([
           'data' => [
               'status'  => 200,
               'message' => 1,
           ],
        ]);
    }

    public function products(Request $request){
        $order_id = $request->order_id;

        $items = [];
        $products = Product::all();
        foreach ($products as $key => $product) {
            if ($product->category_id){
                $category = Category::find($product->category_id);
                $items[$category->title][] = $product;
            }
            else{
                $items['Без категории'][] = $product;
            }
        }

        $order_products = [];

        $results = OrderProduct::where('order_id',$order_id)->get();
        foreach ($results as $key => $value) {
            $order_products[] = $value->product_id;
        }

        if (!$order_products && Session::has('order_products')){ 
            $order_products = Session::get('order_products');
        }

        return Voyager::view('voyager::orders.products', compact('order_id', 'items', 'order_products'));
    }

    public function editProducts(Request $request){
        $order_id = $request->order_id;

        if ($order_id){
            OrderProduct::where('order_id', $order_id)->delete();

            foreach ($request->products as $key => $product) {
                $op = new OrderProduct();
                $op->product_id = $product;
                $op->order_id = $order_id;
                $op->save();
            }

            return response()->json([
               'data' => [
                   'status'  => 200,
                   'message' => 1,
               ],
            ]);
        }
        else{
            $products = [];
            if ($request->products){
                foreach ($request->products as $key => $product) {
                    $products[] = $product;
                }
            }

            Session::put('order_products', $products);

            return response()->json([
               'data' => [
                   'status'  => 200,
                   'message' => 1,
               ],
            ]);
        }
    }

    public function discount_new(Request $request){
        $discount_sum = $request->discount_sum;
        $discount_percent = $request->discount_percent;

        Session::put('order_discount', ['discount_sum' => $discount_sum, 'discount_percent' => $discount_percent]);

        return response()->json([
           'data' => [
               'status'  => 200,
               'message' => 1,
           ],
        ]);
    }
}
