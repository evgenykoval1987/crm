<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\Product;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\OrderProduct;

class VoyagerOrderapiController extends Controller{
	public function index(Request $request){
		
		
		if (!isset($request->key))
			exit;
		if ($request->key != 'FNfLa4tcxZgYGwpB')
			exit;

		$project_id = $request->project_id;
		$client = $request->name;
		$telephone = $request->telephone;

		if (isset($request->comment))
			$ocomment = $request->comment;
		else 
			$ocomment = '';

		if (isset($request->clinic_id))
			$clinic_id = $request->clinic_id;
		else
			$clinic_id = 1;

		$city = '';
		$street = '';
		$flat = '';

		if (isset($request->city))
			$city = $request->city;
		if (isset($request->street))
			$street = $request->street;
		if (isset($request->flat))
			$flat = $request->flat;

		$order = new Order();
		$order->ordertype_id = 1;
		$order->status_id = 1;
		$order->clinic_id = $clinic_id;
		$order->project_id = $project_id;
		$order->executiontype_id = 2;
		$order->client = $client;
		$order->telephone = $telephone;
		$order->ocomment = $ocomment;
		$order->address_city = $city;
		$order->address_street = $street;
		$order->address_intercom = $flat;
		$order->issue_dt = now();
		$order->save();

		$order_id = $order->id;

		if (isset($request->items)){
			$items = json_decode($request->items);
			foreach ($items as $key => $item) {
				
				$product_info = Product::where('outer_id', $item->offer->externalId)->where('project_id', $project_id)->first();
				if ($product_info){
					$op = new OrderProduct();
	                $op->product_id = $product_info->id;
	                $op->order_id = $order_id;
	                $op->save();
				}
			}
		}

		echo 1;
	}
}