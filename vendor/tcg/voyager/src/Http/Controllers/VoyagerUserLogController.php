<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\UserLog;

class VoyagerUserLogController extends VoyagerBaseController
{
    /*public function profile(Request $request)
    {
        return Voyager::view('voyager::profile');
    }*/

    public function index(Request $request){
        $items = UserLog::all();
        return parent::index($request);
    }
}
