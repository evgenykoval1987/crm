<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Shift;
use PDF;


class VoyagerShiftController extends VoyagerBaseController
{
  
    public function index(Request $request){
        
        return parent::index($request);
    }

    public function report(Request $request, $id){ 
    	$data = array();

    	$shift = Shift::find($id);



    	$data['date_begin'] = date('d.m.Y', strtotime($shift->begin_dt));
    	$data['courier'] = $shift->user->name;

    	$data['time_begin'] = date('H:i', strtotime($shift->begin_dt));
    	$data['date_end'] = date('d.m.Y H:i', strtotime($shift->end_dt));

    	$data['shift'] = $shift;
    	$data['orders'] = [];
    	$data['total'] = 0;
    	$data['crem'] = 0;
    	$data['evt'] = 0;
    	foreach ($shift->orders as $order){



    		$order->total = $order->total($order->id);



    		$services = [];
    		foreach ($order->products as $key => $item) {  			
    			$services[] = $item->product->title;
    		}

    		$data['orders'][] = [
    			'order_id' => $order->id,
    			'total' => $order->total,
                'start_dt' => $order->receive_dt,
    			'finish_dt' => $order->close_dt,
    			'services' => $services
    		];

    		$data['total'] += $order->total;

            if ($order->total != 0){
                foreach ($order->products as $key => $product) {
                    if (strpos($product->product->title, 'Пакет') !== false){
                        $data['evt']++; 
                    }
                    else if (strpos($product->product->title, 'Усыпление') !== false || strpos($product->product->title, 'усыпление') !== false){
                        $data['evt']++;
                    }
                    else if (strpos($product->product->title, 'кремация') !== false || strpos($product->product->title, 'Кремация') !== false){
                        $data['crem']++;
                    }
                }
            }
    	}

    	$data['dop'] = 0;
    	foreach ($shift->shiftcosts as $cost){
    		$data['dop'] += (int)$cost->text;
    	}

        $pdf = PDF::loadView('voyager::shifts.pdf', $data);
        return $pdf->stream('voyager::shifts.pdf');

    	return Voyager::view('voyager::shifts.report', $data);
    }
}
