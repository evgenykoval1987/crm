<?php

namespace TCG\Voyager\Listeners;

use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\OrderLog;
use TCG\Voyager\Models\OrderProduct;
use TCG\Voyager\Http\Controllers\VoyagerTelegramController;
use Session;

class AddBreadOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Create a Order for a given BREAD.
     *
     * @param BreadDataAdded $event
     *
     * @return void
     */
    public function handle(BreadDataAdded $bread)
    {
        if ($bread->dataType['name'] == 'orders'){
            $log = new OrderLog();
            $log->order_id = $bread->data['id'];
            $log->from_status_id = 0;
            $log->to_status_id = $bread->data['status_id'];
            $log->save();

            if (isset($bread->data['telegram_status'])){
                if ($bread->data['telegram_status'] == 'new'){
                    $telegram = new VoyagerTelegramController();
                    $telegram->sendOrder($bread->data->id);
                }
            }

            if ($products = Session::get('order_products')){
                foreach ($products as $key => $product) { 
                    $order_product = new OrderProduct();
                    $order_product->order_id = $bread->data['id'];
                    $order_product->product_id = $product;
                    $order_product->save();
                }
                Session::forget('order_products');
            }
        }
    }
}
