<?php

namespace TCG\Voyager\Listeners;

use TCG\Voyager\Events\BreadDataChanged;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerTelegramController;

class ChangeBreadOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Create a Order for a given BREAD.
     *
     * @param BreadDataAdded $event
     *
     * @return void
     */
    public function handle(BreadDataChanged $bread)
    { 
        if ($bread->dataType['name'] == 'orders'){

            if ($bread->changeType != 'Added' && $bread->changeType != 'Deleted'){
                $changes = $bread->data->getChanges();
                if (isset($changes['status_id'])){
                    if ($changes['status_id'] == '2' || $changes['status_id'] == '4'){
                        //telegram push
                        $telegram = new VoyagerTelegramController();
                        $telegram->sendOrder($bread->data->id);
                    }
                }
            }
        }
    }
}
