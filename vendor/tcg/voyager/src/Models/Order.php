<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\OrderProduct;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\Statustype;
use TCG\Voyager\Models\Clinic;
use TCG\Voyager\Models\User;

class Order extends Model
{
	public function status(){
		return $this->hasOne(Statustype::class, 'id', 'status_id');
	}
	public function clinic(){
		return $this->hasOne(Clinic::class, 'id', 'clinic_id');
	}
    public function products(){
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }
    public function vet(){
		return $this->hasOne(User::class, 'id', 'vet_id');
	}

    public function total($order_id){
    	$sum = 0;
    	$products = OrderProduct::where('order_id', $order_id)->get();

    	foreach($products as $product){
			if ($product->product->special == '' || $product->product->special == 0){
				$sum += $product->product->price;
			}
			else{
				$sum += $product->product->special;
			}
		}

		$order = Order::find($order_id);
		if (isset($order->discount_sum)){
			if ($order->discount_sum != '' && $order->discount_sum != 0){
				$sum -= (float)$order->discount_sum;
			}
			else if ($order->discount_percent != '' && $order->discount_percent != 0){
				$sum -= ($sum*($order->discount_percent/100));
			}
		}

		return $sum;
    }
}
