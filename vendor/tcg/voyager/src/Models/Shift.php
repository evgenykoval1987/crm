<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\ShiftCost;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\User;
use Illuminate\Database\Eloquent\Builder;

class Shift extends Model
{
    public $timestamps = false;

    protected static function boot(){
  		parent::boot();
      	static::addGlobalScope('shift', function (Builder $builder) {
          	$builder->orderBy('begin_dt', 'DESC');
      	});
    }

    public function shiftcosts(){
        return $this->hasMany(ShiftCost::class, 'shift_id', 'id');
    }

    public function orders(){
        return $this->hasMany(Order::class, 'shift_id', 'id');
    }

    public function user(){
      return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
