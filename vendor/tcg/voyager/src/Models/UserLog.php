<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Statustype;

class UserLog extends Model
{
    public function scopeOrdered($query){
	    return $query->orderBy('created_at', 'desc');
	}
}
