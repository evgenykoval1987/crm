<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Statustype;

class OrderLog extends Model
{
    public function scopeOrdered($query){
	    return $query->orderBy('created_at', 'desc');
	}

	public function fromstatus(){
		return $this->hasOne(Statustype::class, 'id', 'from_status_id');
	}

	public function tostatus(){
		return $this->hasOne(Statustype::class, 'id', 'to_status_id');
	}
}
