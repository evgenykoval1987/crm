<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Order;
use TCG\Voyager\Models\Product;

class OrderProduct extends Model
{
    public function order(){
        return $this->belongsTo(Order::class,'id', 'order_id');
    }

    public function product(){
        return $this->hasOne(Product::class,'id', 'product_id');
    }
}
