<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Shift;


class ShiftCost extends Model
{
    public $timestamps = false;

    public function group(){
    	return $this->belongsTo(Shift::class, 'id', 'shift_id');
  	}
}
